public class Pessoa {
    private String Cpf;
    private String nome;

    public Pessoa(String cpf, String nome) {
        Cpf = cpf;
        this.nome = nome;
    }

    public String getCpf() {
        return Cpf;
    }

    @Override
    public String toString() {
        return "Pessoa [Cpf=" + Cpf + ", nome=" + nome + "]";
    }

    @Override
    public boolean equals(Object outraPessoa) {
        return this.Cpf.equals(((Pessoa) outraPessoa).getCpf());
    }
}
