public class DinamicList<T> {

  private No<T> inicio;
  private No<T> fim;
  private int tamanho; // opcional

  public DinamicList() {
    this.inicio = null;
    this.fim = null;
    this.tamanho = 0;
  }

  // length usando tamanho
  public int length() {
    return this.tamanho;
  }

  public void print() {
    No<T> temp = this.inicio;
    for (int i = 0; i < length(); i++) {
      System.out.println("lista[" + i + "] = " + temp.getDado());
      temp = temp.getProx();
    }
  }

  // boolean pq poderia ter tratamento de exceção na criação do elemento
  private boolean inserePrimeiroElemento(T valor) {
    No<T> elemento = new No<T>(valor);
    this.inicio = elemento;
    this.fim = elemento;
    this.tamanho++;
    return true;
  }

  private boolean insereInicio(T valor) {
    No<T> elemento = new No<T>(valor);
    elemento.setProx(this.inicio);
    this.inicio = elemento;
    this.tamanho++;
    return true;
  }

  private boolean insereFinal(T valor) {
    No<T> elemento = new No<T>(valor);
    elemento.setProx(null);
    this.fim.setProx(elemento);
    this.fim = elemento;
    this.tamanho++;
    return true;
  }

  private boolean insereMeio(T valor, int pos) {
    No<T> elemento = new No<T>(valor);
    No<T> aux = this.inicio; // aux le a posição inicial
    No<T> temp = this.inicio.getProx(); // temp a prox posição;

    if ((pos < 0) || (pos > length())) {
      return false;
    } else {
      for (int i = 1; i < pos; i++) {
        aux = temp;
        temp = temp.getProx();
      }
      aux.setProx(elemento);
      elemento.setProx(temp); // elemento.setProx(aux.getProx());
      this.tamanho++;
      return true;
    }
  }

  public boolean insert(T valor, int pos) {
    if ((pos < 0) || (pos > length())) {
      return false;
    } else if ((pos == 0) && (length() == 0)) {
      return inserePrimeiroElemento(valor);
    } else if ((pos == 0) && (length() > 0)) {
      return insereInicio(valor);
    } else if (pos == length()) {
      return insereFinal(valor);
    } else {
      return insereMeio(valor, pos);
    }
  }

  public boolean isEmpty() {
    return inicio == null;
  }

  public No<T> removeInicio() {
    if (isEmpty()) {
      return null;
    }
    No<T> temp = inicio;
    inicio = inicio.getProx();
    if (inicio == null) {
      fim = null;
    }
    tamanho--;
    temp.setProx(null);
    return temp;
  }

  public No<T> removeFinal() {
    if (isEmpty()) {
      return null;
    }
    if (inicio == fim) { // se temos apenas um No
      return removeInicio();
    }
    No<T> temp = inicio;
    while (temp.getProx() != fim) {
      temp = temp.getProx();
    }
    fim = temp;
    temp = temp.getProx();
    fim.setProx(null);
    tamanho--;
    return temp;
  }

  public T getDado(int posicao) {
    if (posicao < 0 || posicao > tamanho) {
      return null;
    }
    if (isEmpty()) {
      return null;
    }
    No<T> aux = inicio;
    for (int i = 1; i <= posicao; i++) { // move até o anterior do ná a ser removido
      aux = aux.getProx();
    }
    return aux.getDado();
  }

  public No<T> removePosicao(int posicao) {
    if (posicao < 0 || posicao > tamanho) {
      return null;
    }
    if (isEmpty()) {
      return null;
    }
    if (posicao == 0) {
      return removeInicio();
    }
    if (posicao == tamanho) {
      return removeFinal();
    }
    No<T> aux = inicio;
    for (int i = 1; i < posicao; i++) { // move até o anterior do ná a ser removido
      aux = aux.getProx();
    }
    No<T> temp = aux.getProx();
    aux.setProx(temp.getProx());
    tamanho--;
    temp.setProx(null);
    return temp;
  }

  public boolean exist(T dado) {
    if (isEmpty()) {
      return false;
    }
    No<T> aux = inicio;
    while (aux != null) {
      if (aux.getDado().equals(dado)) {
        return true;
      }
      aux = aux.getProx();
    }
    return false;
  }

}