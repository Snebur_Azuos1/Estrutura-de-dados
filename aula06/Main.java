class Main {
  public static void main(String[] args) {

    DinamicList<Double> minhalista = new DinamicList<>();
    minhalista.insert(5.0, 0);
    minhalista.insert(8.0, 1);
    minhalista.insert(50.0, 2);
    minhalista.insert(888.0, 3);
    minhalista.insert(675.0, 4);
    minhalista.insert(150.0, 5);
    minhalista.insert(951.0, minhalista.length());

    // System.out.println(minhalista.exist(5.0));
    // System.out.println(minhalista.exist(6.0));
    // System.out.println(minhalista.exist(888.0));
    // System.out.println(minhalista.exist(951.0));
    // minhalista.print();

    // minhalista.removePosicao(3);
    // minhalista.print();

    DinamicList<Pessoa> listaPessoa = new DinamicList<>();

    listaPessoa.insert(new Pessoa("111", "aaa"), 0);
    listaPessoa.insert(new Pessoa("222", "bbb"), 1);
    listaPessoa.insert(new Pessoa("333", "ccc"), 2);
    listaPessoa.insert(new Pessoa("444", "ddd"), 3);

    System.out.println(listaPessoa.exist(new Pessoa("111", "aaa")));
    System.out.println(listaPessoa.exist(new Pessoa("444", "aaa")));
    System.out.println(listaPessoa.exist(new Pessoa("123", "aaa")));
    // listaPessoa.removePosicao(2);
    // listaPessoa.print();
    // System.out.println(listaPessoa);

    System.out.println(listaPessoa.getDado(2));
  }
}