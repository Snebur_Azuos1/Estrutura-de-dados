package alocacao;

public class Pessoa {
    private String nome;
    private Pessoa proximo;

    public Pessoa(String nome) {
        this.nome = nome;
        proximo = null;
    }

    public String getNome() {
        return nome;
    }

    public Pessoa getProximo() {
        return proximo;
    }

    public void setProximo(Pessoa proximo) {
        this.proximo = proximo;
    }
}
