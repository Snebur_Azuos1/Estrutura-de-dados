package alocacao;

public class AppAloca {
    public static void main(String[] args) {
        Pessoa primeiraPessoa = null;

        primeiraPessoa = new Pessoa("Carlos");

        System.out.println(primeiraPessoa.getNome());

        primeiraPessoa.setProximo(new Pessoa("Amanda"));
        primeiraPessoa.getProximo().setProximo(new Pessoa("João"));

        System.out.println(primeiraPessoa.getProximo().getNome());
        System.out.println(primeiraPessoa.getProximo().getProximo().getNome());
    }
}
