package recursivo2;

/**
 * Fibonacci
 */
public class AppFibonacci {
   private static int resp[];

   public static void main(String[] args) {
      int n = 50;
      resp = new int[n];
      resp[0] = 0;
      resp[1] = 1;

      System.out.println("I:" + fibonacci(n));
      System.out.println("R:" + fibonacciR(n));
   }

   static public int fibonacciR(int n) {
      if (n < 2) { // caso base
         return n;
      }
      if (resp[n - 1] == 0)
         resp[n - 1] = fibonacciR(n - 1);
      if (resp[n - 2] == 0)
         resp[n - 2] = fibonacciR(n - 2);

      return resp[n - 1] + resp[n - 2];
   }

   static public int fibonacci(int n) {
      int i, atual = 0, ant1, ant2;

      if ((n == 0) || (n == 1))
         atual = n;
      else {
         ant1 = 0;
         ant2 = 1;
         for (i = 2; i <= n; i++) {
            atual = ant1 + ant2;
            ant1 = ant2;
            ant2 = atual;
         }
      }
      return (atual);
   }

}