public interface Robo {
    boolean andar(Direcao direcao, int deslocamento);

    String toString();
}
