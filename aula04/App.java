public class App {
    public static void main(String[] args) {
        Area a1 = new Area(10, 10);
        Robo r1 = new RoboSimples(a1);
        Robo r2 = new RoboTanque(a1);

        Robo[] robos = new Robo[2];
        robos[0] = r1;
        robos[1] = r2;

        // System.out.println(r1);
        // r1.andar(Direcao.OESTE, 2);
        // System.out.println(r1);
        // r1.andar(Direcao.LESTE, 2);
        // System.out.println(r1);
        // r2.andar(Direcao.LESTE, 2);
        // System.out.println(r2);

        for (Robo robo : robos) {
            robo.andar(Direcao.LESTE, 2);
            System.out.println(robo);
        }

    }
}
