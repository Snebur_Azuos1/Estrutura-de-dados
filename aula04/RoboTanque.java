public class RoboTanque implements Robo {
    private int x, y;
    private Area area;

    public RoboTanque(Area area) {
        this.area = area;
    }

    @Override
    public boolean andar(Direcao direcao, int deslocamento) {
        if (area.naoValida(x, y, direcao, deslocamento)) {
            return false;
        }
        switch (direcao) {
            case NORTE:
                y -= deslocamento / 2;
                break;
            case SUL:
                y += deslocamento / 2;
                break;
            case OESTE:
                x -= deslocamento / 2;
                break;
            case LESTE:
                x += deslocamento / 2;
                break;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RoboTanque [x=" + x + ", y=" + y + "]";
    }

}
