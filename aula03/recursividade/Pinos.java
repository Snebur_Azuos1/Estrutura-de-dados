package recursividade;

import java.util.Scanner;

public class Pinos {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int numLinhas, totalPinos;

        System.out.println("Digite o número de linhas:");
        numLinhas = entrada.nextInt();

        totalPinos = calculaPinosRec(numLinhas);

        System.out.println("#Pinos = " + totalPinos);
        entrada.close();
    }

    private static int calculaPinos(int numLinhas) {
        int somaPinos = 0;

        for (int i = 1; i <= numLinhas; i++) {
            somaPinos += i;
        }

        return somaPinos;
    }

    private static int calculaPinosRec(int numLinhas) {
        // caso base
        if (numLinhas <= 1)
            return 1;
        // caso recursivo
        return numLinhas + calculaPinosRec(numLinhas - 1);
    }

}
