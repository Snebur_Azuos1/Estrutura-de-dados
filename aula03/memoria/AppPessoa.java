package memoria;

public class AppPessoa {
    public static void main(String[] args) {
        Pessoa p1 = null;
        Pessoa p2;

        p1 = new Pessoa();
        p1.nome = "Marcos";
        p1.idade = 23;

        p2 = p1;
        p2.idade = 43;

        System.out.println(p1);
        System.out.println(p2);
    }
}
