import java.util.Scanner;

/**
 * Exerc01
 */
public class Exerc01 {

    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int valor;

        System.out.println("Digite um valor inteiro positivo:");
        valor = entrada.nextInt();

        System.out.print(valor);
        while (valor > 1) {
            if (valor % 2 == 0) {
                valor /= 2;
            } else {
                valor = valor * 3 + 1;
            }
            System.out.print(" -> " + valor);
        }
        entrada.close();
    }
}