package exercicios;

import java.util.Scanner;

import stack.MyStack;

/**
 * Exercicio01: Segunda solução
 * Leia uma frase e mostre a frase com cada palavra invertida.
 */

public class Exercicio01B {

    public static void main(String[] args) {
        MyStack<Character> stack = new MyStack<>();
        Scanner input = new Scanner(System.in);
        String frase;
        String palavras[];

        System.out.println("Digite a frase:");
        frase = input.nextLine();

        palavras = frase.split(" ");

        for (String palavra : palavras) {
            for (Character letra : palavra.toCharArray()) {
                stack.push(letra);
            }
            while (!stack.isEmpty()) {
                System.out.print(stack.pop());
            }
            System.out.print(" ");
        }

        input.close();
    }
}