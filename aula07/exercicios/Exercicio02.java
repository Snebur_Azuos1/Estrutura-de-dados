package exercicios;

import java.util.Scanner;

import stack.MyStack;

public class Exercicio02 {
    public static void main(String[] args) {
        MyStack<Character> stack = new MyStack<>();
        Scanner input = new Scanner(System.in);
        String expressao;

        System.out.println("Digite a expressão:");
        expressao = input.nextLine();
        input.close();

        for (Character simbolo : expressao.toCharArray()) {
            if (simbolo == '(' || simbolo == '[' || simbolo == '{') {
                stack.push(simbolo);
                continue;
            }
            if (simbolo == ')') {
                if (stack.isEmpty()) {
                    System.out.println("Expressão incorreta. Falta (");
                    return;
                }
                if (stack.pop() != '(') {
                    System.out.println("Expressão incorreta");
                    return;
                }
            } else {
                if (simbolo == ']') {
                    if (stack.isEmpty()) {
                        System.out.println("Expressão incorreta. Falta [");
                        return;
                    }
                    if (stack.pop() != '[') {
                        System.out.println("Expressão incorreta");
                        return;
                    }
                } else {
                    if (simbolo == '}') {
                        if (stack.isEmpty()) {
                            System.out.println("Expressão incorreta. Falta {");
                            return;
                        }
                        if (stack.pop() != '}') {
                            System.out.println("Expressão incorreta");
                            return;
                        }
                    }
                }
            }
        }
        if (!stack.isEmpty()) {
            System.out.println("Expressão incorreta. Sobraram:");
            while (!stack.isEmpty()) {
                System.out.print(stack.pop() + " ");
            }
        } else {
            System.out.println("Expressão correta.");
        }

    }
}
