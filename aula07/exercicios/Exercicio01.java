package exercicios;

import java.util.Scanner;

import stack.MyStack;

/**
 * Exercicio01:
 * Leia uma frase e mostre a frase com cada palavra invertida.
 */

public class Exercicio01 {

    public static void main(String[] args) {
        MyStack<Character> stack = new MyStack<>();
        Scanner input = new Scanner(System.in);
        String frase;

        System.out.println("Digite a frase:");
        frase = input.nextLine();

        for (int i = 0; i < frase.length(); i++) {
            char letra = frase.charAt(i);
            if (letra != ' ') {
                stack.push(letra);
            }
            if (letra == ' ' || i == frase.length() - 1) {
                while (!stack.isEmpty()) {
                    System.out.print(stack.pop());
                }
                System.out.print(" ");
            }
        }

        input.close();
    }
}