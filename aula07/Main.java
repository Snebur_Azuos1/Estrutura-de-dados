import stack.MyStack;

class Main {
  public static void main(String[] args) {

    MyStack<Double> minhaPilha = new MyStack<>();

    minhaPilha.push(1.0);
    minhaPilha.push(2.0);
    minhaPilha.push(3.0);
    minhaPilha.push(4.0);
    minhaPilha.push(5.0);

    while (!minhaPilha.isEmpty()) {
      System.out.println(minhaPilha.pop());
    }
  }
}