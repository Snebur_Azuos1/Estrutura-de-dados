package stack;

public class MyStack<T> {

  private No<T> topo;
  private int tamanho;

  public MyStack() {
    this.topo = null;
    this.tamanho = 0;
  }

  public int size() {
    return this.tamanho;
  }

  public void print() {
    No<T> temp = this.topo;
    for (int i = 0; i < size(); i++) {
      System.out.println("pilha[" + i + "] = " + temp.getDado());
      temp = temp.getProx();
    }
  }

  public boolean push(T valor) {
    No<T> elemento = new No<T>(valor);
    elemento.setProx(this.topo);
    this.topo = elemento;
    this.tamanho++;
    return true;
  }

  public boolean isEmpty() {
    return topo == null;
  }

  public T pop() {
    if (isEmpty()) {
      return null;
    }
    No<T> temp = topo;
    topo = topo.getProx();
    tamanho--;
    return temp.getDado();
  }

  public T peek() {
    if (isEmpty()) {
      return null;
    }
    return topo.getDado();
  }

  public boolean exist(T dado) {
    if (isEmpty()) {
      return false;
    }
    No<T> aux = topo;
    while (aux != null) {
      if (aux.getDado().equals(dado)) {
        return true;
      }
      aux = aux.getProx();
    }
    return false;
  }

}