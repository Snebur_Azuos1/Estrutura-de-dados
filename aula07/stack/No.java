package stack;

public class No<T> {
	private T dado;
	private No<T> prox;

	public No(T dado) {
		this.dado = dado;
		this.prox = null;
	}

	public T getDado() {
		return this.dado;
	}

	public No<T> getProx() {
		return this.prox;
	}

	public void setDado(T valor) {
		this.dado = valor;
	}

	public void setProx(No<T> aux) {
		this.prox = aux;
	}
}
