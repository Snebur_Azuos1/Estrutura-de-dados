package br.paduan;

public class BstTree {
    private Node root;

    public boolean isEmpty() {
        return (root == null);
    }

    public boolean add(int value) {
        if (isEmpty()) {
            root = new Node(value);
            return true;
        }
        Node current = root;
        while (true) {
            if (current.getValue() == value) {
                return false;
            }
            if (value < current.getValue()) {
                if (current.getLeft() != null) {
                    current = current.getLeft();
                } else {
                    current.setLeft(new Node(value));
                    return true;
                }
            } else {
                if (current.getRight() != null) {
                    current = current.getRight();
                } else {
                    current.setRight(new Node(value));
                    return true;
                }
            }
        }
    }

    public void remove(int value) {
        remove(root, value);
    }

    private Node remove(Node root, int value) {
        if (root == null) {
            return null;
        }

        if (value < root.getValue()) {
            root.setLeft(remove(root.getLeft(), value));
        } else {
            if (value > root.getValue()) {
                root.setRight(remove(root.getRight(), value));
            } else {
                if (root.getLeft() == null) { // não tem o filho da esquerda
                    return root.getRight();
                } else {
                    if (root.getRight() == null) {
                        return root.getLeft();
                    }
                }

                // tem dois filhos
                // troca o valor a ser removido, pelo menor valor da sub árvore a direita
                root.setValue(minValue(root.getRight()));

                // remove recursivamente o nó que foi copiado
                root.setRight(remove(root.getRight(), root.getValue()));
            }

        }
        return root;
    }

    private int minValue(Node root) {
        int min = root.getValue();
        while (root.getLeft() != null) {
            root = root.getLeft();
            min = root.getValue();
        }
        return min;
    }

    public boolean search() {
        // TODO: implement
        return true;
    }

    public String showInOrder() {
        return showInOrder(root);
    }

    private String showInOrder(Node node) {
        String out = "";
        if (node == null)
            return out;
        out += showInOrder(node.getLeft()) + " ";
        // System.out.println(node.getValue());
        out += node.getValue() + " ";
        out += showInOrder(node.getRight()) + " ";
        return out;
    }

}
