package br.paduan;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        BstTree tree = new BstTree();

        System.out.println("Arvore vazia:" + tree.isEmpty());
        tree.add(10);
        tree.add(5);
        tree.add(15);
        tree.add(7);
        tree.add(6);
        tree.add(4);
        System.out.println("Arvore vazia:" + tree.isEmpty());
        System.out.println(tree.showInOrder());
        tree.remove(10);

        System.out.println(tree.showInOrder());
    }
}
