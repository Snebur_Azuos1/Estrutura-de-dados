package br.paduan;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Node {
    private int value;
    private Node left, right;

    public Node(int value) {
        this.value = value;
    }
}
